let token = localStorage.getItem("token");
let profileContainer = document.querySelector("#profileContainer")
let courseIds = []
let courseNames = []
let userData = {}
let coursesList;

// fetch logged-in user
fetch('https://greatest-bandster.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	
	 let dataContain = `<div class="container" data-aos="zoom-out-down">
		<div id="dataContain" class="row">
		<div class="col">
		<section class="jumbotron text-center my-5 border border-danger">
		<h1 class="myPro"><strong>MY PROFILE</strong></h1>
		<br>
		<div class="thumb">
		<img src="../assets/images/profile_pic.jpg">
		</div>
		<h3><strong>NAME:</strong></h3>
		<h3>${data.firstName} ${data.lastName}</h3>
		<br>
		<h3><strong>EMAIL:</strong></h3>
		<h3>${data.email}</h3>
		<br>
		<h3><strong>CONTACT NO:</strong></h3>
		<h3>${data.mobileNo}</h3>
		<br>`

	for(let i = 0; i < data.enrollments.length; i++){
		courseIds.push(data.enrollments[i].courseId)
	}

	fetch('https://greatest-bandster.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		for(let i = 0; i < data.length; i++){

			if(courseIds.includes(data[i]._id)){
				courseNames.push(data[i].name)
			}
		}

		coursesList = courseNames.join("<br>")
  //Course names are here in coursesList
  let coursesContain = `<h3><strong>COURSES ENROLLED:</strong></h3>
		<h3>${coursesList}</h3>
		</section>
		</div>`
 

		profileContainer.innerHTML = dataContain + coursesContain
	})
})

