let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem("token");

let archCourse;
fetch(`https://greatest-bandster.herokuapp.com/api/courses/active/${courseId}`, {
	method: "DELETE",
	headers: {
		"Content-Type": "application/json",
		"Authorization": `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => {
	return res.json()
})
.then(data => {
	// console.log(data)
	if(data === !true){
		console.log("Unsuccessful!")
		
	}else{

		alert("Course Activated!")
		window.location.replace("./courses.html")
	}

	
	console.log(archCourse)
})