let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem("token");
let disable = document.querySelector("#disable")

let archCourse;
fetch(`hhttps://greatest-bandster.herokuapp.com/api/courses/${courseId}`, {
	method: "DELETE",
	headers: {
		"Content-Type": "application/json",
		"Authorization": `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => {
	return res.json()
})
.then(data => {
	// console.log(data)
	if(data === !true){
		console.log("Unsuccessful!")
		
	}else{

		archCourse = `<h2 class="my-5" id="disable">Course Disabled!</h2>

			<a href="./courses.html" class="btn btn-outline-danger"> Go Back to the Courses page</a>`
			disable.innerHTML = archCourse
	}

	
	console.log(archCourse)
})