let navItems =  document.querySelector("#navSession")
let navProfile = document.querySelector("#aaron")
let fname;

let userToken = localStorage.getItem("token")



if(!userToken){
	navItems.innerHTML = `<li class="nav-item">
	<a href="./login.html" class="nav-link">Log In</a>
	</li>`
}else{
	fetch("https://greatest-bandster.herokuapp.com/api/users/details", {
		headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${userToken}`
		}
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		fname = data.firstName
		navProfile.innerHTML = `<li class="nav-item active">
		<a href="./profile.html" class="nav-link"> Hello ${fname}!</a>
		</li>`
		navItems.innerHTML = `<li class="nav-item">
		<a href="./logout.html" class="nav-link">Log Out</a>
		</li>`
	})
	
}