let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem("token");
let disable = document.querySelector("#disable")

let archCourse;
fetch(`https://greatest-bandster.herokuapp.com/api/courses/${courseId}`, {
	method: "DELETE",
	headers: {
		"Content-Type": "application/json",
		"Authorization": `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => {
	return res.json()
})
.then(data => {
	// console.log(data)
	if(data === !true){
		console.log("Unsuccessful!")
		
	}else{

		alert("Course Deactivated!")
		window.location.replace("./courses.html")
	}

	
	console.log(archCourse)
})